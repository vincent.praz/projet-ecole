package com.crea.dev2.a2.jee.ecole.model.dao;

import com.crea.dev2.a2.jee.ecole.model.beans.Livre;
import com.crea.dev2.a2.jee.ecole.model.utils.DBAction;
import com.mysql.jdbc.PreparedStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

public class LivreDAO {
    static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    /**
     * Récupète un livre selon sa cote
     *
     * @param cote cote du livre
     * @return le livre récupéré
     * @throws SQLException
     */
    public static Livre getLivreByCote(String cote) throws SQLException {
        DBAction.DBConnexion(); // test conn
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM livre WHERE cote = '" + cote + "';");
        resultSet.first(); // Starting the resultset at the first position
        Livre livreTemp = new Livre(resultSet.getString("cote"), resultSet.getString("num"), resultSet.getString("titre"), resultSet.getDate("datepret"));
        resultSet.close(); // Fermeture du resultSet
        DBAction.DBClose();
        return livreTemp;
    }

    /**
     * Supprime le livre selon sa cote
     *
     * @param cote cote du livre à supprimer
     * @return 1 ou 0 (le nbr de livre supprimés) sinon le (-) code d'erreur
     */
    public static int deleteLivreByCote(String cote) {
        int result; // résultat de la requête
        DBAction.DBConnexion(); // test conn
        try {
            result = DBAction.getStm().executeUpdate("DELETE FROM livre WHERE cote = '" + cote + "';");
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }

    /**
     * Ajoute un Livre
     *
     * @param cote     cote du livre
     * @param num      numéro d'identifiant de l'élève
     * @param titre    titre du livre
     * @param datepret date de prêt du livre
     * @return 1 ou 0 (le nbr de livre ajouté) sinon le (-) code d'erreur
     * @throws ParseException
     */
    public static int addLivre(String cote, String num, String titre, Date datepret) throws ParseException {
        DBAction.DBConnexion(); // test conn
        int result; //résultat de la requête
        // Attention a bien mettre des '' pour les strings


        String SQL = "INSERT INTO livre(cote,num,titre,datepret) VALUES('" + cote + "','" + num + "','" + titre + "','" + df.format(datepret) + "');";
        try {
            result = DBAction.getStm().executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            result = e.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }


    /**
     * Récupère le vecteur de l'entièreté des livres
     *
     * @return le vecteur contenant tous les livres
     * @throws SQLException
     */
    public static Vector<Livre> getAllLivre() throws SQLException {
        DBAction.DBConnexion(); // test conn
        Vector<Livre> vLivre = new Vector<>();
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM livre;");
        while (resultSet.next()) {
            Livre livreTemp = new Livre(resultSet.getString("cote"), resultSet.getString("num"), resultSet.getString("titre"), resultSet.getDate("datepret"));
            vLivre.add(livreTemp);
        }
        DBAction.DBClose();
        return vLivre;
    }


    /**
     * Modifie le num selon la cote
     *
     * @param cote cote du livre
     * @param num  nouveau numéro identifiant de l'élève
     * @return
     * @throws SQLException
     */
    public static int updateNumByCote(String cote, String num) throws SQLException {
        int result;
        DBAction.DBConnexion();
        String req = "UPDATE livre SET num = '" + num + "' WHERE cote ='" + cote + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }

        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Modifie le titre d'un livre selon sa cote
     *
     * @param cote  cote du livre
     * @param titre nouveau titre du livre
     * @return
     * @throws SQLException
     */
    public static int updateTitreByCote(String cote, String titre) throws SQLException {
        int result;
        DBAction.DBConnexion();
        String req = "UPDATE livre SET titre = '" + titre + "' WHERE cote ='" + cote + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }

        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }
}
