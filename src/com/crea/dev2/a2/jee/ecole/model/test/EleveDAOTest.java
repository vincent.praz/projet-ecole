package com.crea.dev2.a2.jee.ecole.model.test;

import com.crea.dev2.a2.jee.ecole.model.beans.Chambre;
import com.crea.dev2.a2.jee.ecole.model.beans.Eleve;
import com.crea.dev2.a2.jee.ecole.model.dao.ChambreDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static com.crea.dev2.a2.jee.ecole.model.dao.ChambreDAO.*;
import static com.crea.dev2.a2.jee.ecole.model.dao.EleveDAO.*;
import static org.junit.Assert.*;

public class EleveDAOTest {

    /**
     * Before pour insérer des données test dans la database
     */
    @Before
    public void testAddEleve() throws SQLException {
        // Création des élèves dans la database avec un no null
        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
        Eleve e2 = new Eleve("KAMTO005", 2, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
        Eleve e3 = new Eleve("LAURENCY004", 3, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
        Eleve e4 = new Eleve("TABIS003", 4, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
        Eleve e5 = new Eleve("TAHAE002", 5, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

        listEleve.add(e1);
        listEleve.add(e2);
        listEleve.add(e3);
        listEleve.add(e4);
        listEleve.add(e5);


        for (Eleve eleve : listEleve) {
            assertEquals(1, addEleve(eleve.getNum(), eleve.getNom(), eleve.getAge(), eleve.getAdresse()));
        }

        // Création dans la database des chambres avec un num null
        Chambre ch1 = new Chambre(1, "AGUE001", 350);
        Chambre ch2 = new Chambre(2, "KAMTO005", 400);
        Chambre ch3 = new Chambre(3, "LAURENCY004", 50);
        Chambre ch4 = new Chambre(4, "TABIS003", 400);
        Chambre ch5 = new Chambre(5, "TAHAE002", 250);

        ArrayList<Chambre> chambreList = new ArrayList<>();
        chambreList.add(ch1);
        chambreList.add(ch2);
        chambreList.add(ch3);
        chambreList.add(ch4);
        chambreList.add(ch5);

        for (Chambre chambre : chambreList) {
            assertEquals(1, addChambre(chambre.getNo(), chambre.getPrix()));
        }

        // Pour chaque élève, on lui attribue un no de chambre et on attribue à sa chambre le num de l'élève en db
        int i = 1;
        for (Eleve eleve : listEleve) {

            System.out.println("Updating eleve num" + eleve.getNum() + " setting no chambre " + i);
            updNoChambreEleveByNum(eleve.getNum(), i);
            System.out.println("Updating ch " + i + " setting num " + eleve.getNum());
            ChambreDAO.updateNumByNo(eleve.getNum(), i);
            i++;
        }
    }

    /**
     * After pour supprimer les données test de la database
     */
    @After
    public void testDeleteEleveByNum() throws SQLException {
        // On set les no de chambre à null dans la table élève
        updNoChambreEleveByNum("AGUE001");
        updNoChambreEleveByNum("KAMTO005");
        updNoChambreEleveByNum("LAURENCY004");
        updNoChambreEleveByNum("TABIS003");
        updNoChambreEleveByNum("TAHAE002");


        // On set les num d'élève a null dans la table chambre
        updateNumByNo(1);
        updateNumByNo(2);
        updateNumByNo(3);
        updateNumByNo(4);
        updateNumByNo(5);


        // Suppression des élèves
        assertEquals(1, deleteEleveByNum("AGUE001"));
        assertEquals(1, deleteEleveByNum("KAMTO005"));
        assertEquals(1, deleteEleveByNum("LAURENCY004"));
        assertEquals(1, deleteEleveByNum("TABIS003"));
        assertEquals(1, deleteEleveByNum("TAHAE002"));

        // Suppression des chambres
        assertEquals(1, deleteChambreByNo(1));
        assertEquals(1, deleteChambreByNo(2));
        assertEquals(1, deleteChambreByNo(3));
        assertEquals(1, deleteChambreByNo(4));
        assertEquals(1, deleteChambreByNo(5));
    }

    @Test
    public void testGetEleveByNum() throws SQLException {
        Eleve e_ref = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");//"AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris"
        Eleve e_res = new Eleve();
        e_res = getEleveByNum("AGUE001");

        assertEquals(e_ref.getAdresse(), e_res.getAdresse());
        assertEquals(e_ref.getAge(), e_res.getAge());
        assertEquals(e_ref.getNo(), e_res.getNo());
        assertEquals(e_ref.getNom(), e_res.getNom());
        assertEquals(e_ref.getNum(), e_res.getNum());
    }

    @Test
    public void testGetEleveByNom() throws SQLException {
        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");

        listEleve.add(e1);

        for (int i = 0; i < listEleve.size(); i++) {
            System.out.println("Taille Liste : " + getEleveByNom("AGUE MAX").size());
            assertEquals(listEleve.get(i).getAdresse(), getEleveByNom("AGUE MAX").get(i).getAdresse());
            assertEquals(listEleve.get(i).getNo(), getEleveByNom("AGUE MAX").get(i).getNo());
            assertEquals(listEleve.get(i).getNom(), getEleveByNom("AGUE MAX").get(i).getNom());
            assertEquals(listEleve.get(i).getNum(), getEleveByNom("AGUE MAX").get(i).getNum());
            assertEquals(listEleve.get(i).getAge(), getEleveByNom("AGUE MAX").get(i).getAge());
        }
    }

    @Test
    public void testGetEleveByNo() throws SQLException {
        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        Eleve e1 = new Eleve("AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
        Eleve e2 = new Eleve("KAMTO005", 2, "KAMTO Diogène", 50, "54 Rue des Ebisoires 78300 Poissy");
        Eleve e3 = new Eleve("LAURENCY004", 3, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
        Eleve e4 = new Eleve("TABIS003", 4, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
        Eleve e5 = new Eleve("TAHAE002", 5, "TAHA RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");

        listEleve.add(e1);
        listEleve.add(e2);
        listEleve.add(e3);
        listEleve.add(e4);
        listEleve.add(e5);

        System.out.println(listEleve.size());

        // Pour chaque élève dans la liste de test, on va récupérer la liste d'élève avec le même no
        for (int i = 0; i < listEleve.size(); i++) {
            int noActuel = listEleve.get(i).getNo();
            System.out.println("Taille Liste NO : " + getEleveByNo(noActuel).size());
            assertEquals(listEleve.get(i).getAdresse(), getEleveByNo(noActuel).get(0).getAdresse());
            assertEquals(listEleve.get(i).getNo(), getEleveByNo(noActuel).get(0).getNo());
            assertEquals(listEleve.get(i).getNom(), getEleveByNo(noActuel).get(0).getNom());
            assertEquals(listEleve.get(i).getNum(), getEleveByNo(noActuel).get(0).getNum());
            assertEquals(listEleve.get(i).getAge(), getEleveByNo(noActuel).get(0).getAge());
        }
    }


    @Test
    public void testUpdAdresseEleveByNum() throws SQLException {
        // Changement d'adresse
        assertEquals(1, updAdresseEleveByNum("AGUE001", "69 rue de test"));

        // Récupération élève avec nouvelle adresse
        Eleve e1 = getEleveByNum("AGUE001");

        // Vérification du bon changement de l'adresse
        assertEquals("69 rue de test", e1.getAdresse());
    }

    @Test
    public void testUpdNoChambreEleveByNum() throws SQLException {
        // Changement de no
        assertEquals(1, updNoChambreEleveByNum("AGUE001", 2));

        // Récupération élève avec nouveau no
        Eleve e1 = getEleveByNum("AGUE001");

        // Vérification du bon changement du no
        assertEquals(2, e1.getNo());
    }


    @Test
    public void testGetListEleveByDateN() throws SQLException {
        ArrayList<Eleve> elevesByDateN = getListEleveByDateN(1990);

        // Vérification que le tableau n'est pas vide
        assertNotNull(elevesByDateN);
        assertNotEquals(0, elevesByDateN.size());
    }

    @Test
    public void testGetAllEleve() throws SQLException {
        ArrayList<Eleve> eleves = getAllEleve();
        assertNotNull(eleves);
        assertNotEquals(0, eleves.size());
    }

    @Test
    public void testGenerateEleveList() throws SQLException {
        //Récupération liste des élèves
        ArrayList<Eleve> eleves = getAllEleve();

        //Instanciation liste eleves test
        ArrayList<Eleve> elevesTest = new ArrayList<>();

        //Requete test
        String req = "SELECT * FROM eleve;";

        generateEleveList(req, elevesTest);

        // Comparaison taille des tableaux
        assertEquals(eleves.size(), elevesTest.size());
    }
}