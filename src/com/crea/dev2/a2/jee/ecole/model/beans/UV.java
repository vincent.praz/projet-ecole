package com.crea.dev2.a2.jee.ecole.model.beans;

public class UV {
    private String code; // Id code de l'UV
    private int nbh; // Nombre d'heure de cours NOT NULL ON DB
    private String coord; // Coordonnée

    /**
     * Constructeur de l'objet UV
     *
     * @param code  code de l'UV
     * @param nbh   nombre d'heure de cour
     * @param coord coordonnée
     */
    public UV(String code, int nbh, String coord) {
        this.code = code;
        this.nbh = nbh;
        this.coord = coord;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNbh() {
        return nbh;
    }

    public void setNbh(int nbh) {
        this.nbh = nbh;
    }

    public String getCoord() {
        return coord;
    }

    public void setCoord(String coord) {
        this.coord = coord;
    }
}
