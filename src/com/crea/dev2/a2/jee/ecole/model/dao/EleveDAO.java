package com.crea.dev2.a2.jee.ecole.model.dao;

import com.crea.dev2.a2.jee.ecole.model.beans.Eleve;
import com.crea.dev2.a2.jee.ecole.model.utils.DBAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;


public class EleveDAO {
    /**
     * Liste un élève en particulier d'après son identifiant passé en paramètre de la fonction.
     *
     * @param num numéro d'identifiant de l'élève
     * @return : retourne l'objet ElevTemp
     * @throws SQLException
     */
    public static Eleve getEleveByNum(String num) throws SQLException {
        Eleve elevTemp = new Eleve();
        ResultSet res;
        String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE num = '" + num + "' ";
        // Connexion
        DBAction.DBConnexion();//System.out.println(req);
        // exécution de la requête et init
        DBAction.setRes(DBAction.getStm().executeQuery(req));
        while (DBAction.getRes().next()) {
            // Creation de l'objet eleveTemp à travers le ResultSet BD
            elevTemp.setNum(DBAction.getRes().getString(1));
            elevTemp.setNo(DBAction.getRes().getInt(2));
            elevTemp.setNom(DBAction.getRes().getString(3));
            elevTemp.setAge(DBAction.getRes().getInt(4));
            elevTemp.setAdresse(DBAction.getRes().getString(5));
            elevTemp.affiche();
        }
        // Fermeture de la connexion & statement & res
        DBAction.DBClose();
        // Retourner l'objet ElevTemp
        return elevTemp;
    }

    /**
     * Selection d'Eleve(s) par le nom
     *
     * @param listsnoms liste des noms à rechercher
     * @return une liste d'eleve
     * @throws SQLException
     */
    public static ArrayList<Eleve> getEleveByNom(String listsnoms) throws SQLException {
        //Création de ma liste d'élève ayant le meme nom
        ArrayList<Eleve> listEleveNom = new ArrayList<Eleve>();
        String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE nom = '" + listsnoms + "' ";

        // Exécute la requete et remplis la listEleveNom
        generateEleveList(req, listEleveNom);

        for (int i = 0; i < listEleveNom.size(); i++) {
            listEleveNom.get(i).affiche();
        }
        // Fermeture de la connexion
        DBAction.DBClose();
        // Retourner l'objet ElevTemp
        return listEleveNom;
    }


    /**
     * Sélection d'un ou plusieurs éleves par son/leur numéro de chambre
     *
     * @param no numéro de chambre de l'élève
     * @return l'objet eleve récupéré
     * @throws SQLException
     */
    public static ArrayList<Eleve> getEleveByNo(int no) throws SQLException {
        //Création de ma liste d'élève partageant la même chambre
        ArrayList<Eleve> listEleveNo = new ArrayList<Eleve>();

        String req = "SELECT * FROM eleve WHERE no = " + no + ";";

        // Exécute la requête et remplis la liste passée en paramètre
        generateEleveList(req, listEleveNo);

        // Retourner l'objet ElevTemp
        return listEleveNo;
    }

    /**
     * Delete Eleve par un numéro
     *
     * @param num numéro de l'élève
     * @return : 1 ou 0  (le nbr d'étudiants supprimés) sinon le (-) code d'erreur
     */
    public static int deleteEleveByNum(String num) {
        int result = -1;
        DBAction.DBConnexion();
        String req = "DELETE FROM eleve WHERE num = '" + num + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
            System.out.println("Requete executée");
        } catch (SQLException ex) {
            result = -ex.getErrorCode();
            System.out.println(ex.getMessage());
        }
        System.out.println("[" + req + "] Suppression : Valeur de result == " + result);
        DBAction.DBClose();
        return result;
    }


    /**
     * Met à jour de l'adresse d'un élève par son numéro
     *
     * @param num     numéro de l'élève
     * @param adresse adresse de l'élève
     * @return 1 ou 0  (le nbr d'étudiants mis à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updAdresseEleveByNum(String num, String adresse) throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE eleve SET adresse = '" + adresse + "' WHERE num ='" + num + "' ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Met à jour(Attribuer une chambre à 1 élève) le n° de chambre à un élève
     *
     * @param num numéro de l'élève
     * @param no  numéro de chambre
     * @return 1 ou 0  (le nbr d'étudiants mis à jour) sinon le (-) code d'erreur
     */
    public static int updNoChambreEleveByNum(String num, int no) {
        int result = -1;
        DBAction.DBConnexion();

        String req = "UPDATE eleve SET no = " + no + " WHERE num ='" + num + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
            System.out.println("Requete executee");
        } catch (SQLException ex) {
            result = -ex.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }


    /**
     * Met à jour(Attribuer une chambre à 1 élève) le n° de chambre à un élève
     *
     * @param num numéro de l'élève
     * @return 1 ou 0  (le nbr d'étudiants mis à jour) sinon le (-) code d'erreur
     */
    public static int updNoChambreEleveByNum(String num) {
        int result = -1;
        DBAction.DBConnexion();

        String req = "UPDATE eleve SET no = " + null + " WHERE num ='" + num + "' ";
        try {
            result = DBAction.getStm().executeUpdate(req);
            System.out.println("Requete executee");
        } catch (SQLException ex) {
            result = -ex.getErrorCode();
        }
        DBAction.DBClose();
        return result;
    }


    /**
     * Ajouter un élève
     *
     * @param num     numéro de l'élève
     * @param nom     nom de l'élève
     * @param age     age de l'élève
     * @param adresse adresse de l'élève
     * @return 1 ou 0  (le nbr d'étudiants ajouté) sinon le (-) code d'erreur
     */
    public static int addEleve(String num, String nom, int age, String adresse) {
        int result = -1;
        DBAction.DBConnexion();

        String req = "INSERT INTO eleve (num, no, nom, age, adresse)"
                + " VALUES ('" + num + "',NULL,'" + nom + "'," + age + ",'" + adresse + "') ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException ex) {
            result = -ex.getErrorCode();
            System.out.println(ex.getMessage());
        }

        DBAction.DBClose();
        return result;
    }

    /**
     * Ajouter un élève
     *
     * @param num     numéro de l'élève
     * @param nom     nom de l'élève
     * @param age     age de l'élève
     * @param adresse adresse de l'élève
     * @param no      no de chambre de l'élève
     * @return 1 ou 0  (le nbr d'étudiants ajouté) sinon le (-) code d'erreur
     */
    public static int addEleve(String num, String nom, int age, String adresse, int no) {
        int result = -1;
        DBAction.DBConnexion();

        String req = "INSERT INTO eleve (num, no, nom, age, adresse)"
                + " VALUES ('" + num + "'," + no + ",'" + nom + "'," + age + ",'" + adresse + "') ";
        try {
            result = DBAction.getStm().executeUpdate(req);
        } catch (SQLException ex) {
            result = -ex.getErrorCode();
            System.out.println(ex.getMessage());
        }
        //System.out.println("["+req+"] Valeur de result == "+result);
        //System.out.println(req);
        DBAction.DBClose();
        return result;
    }

    /**
     * Renvoie une liste d'élèves en fonction de l'age.
     *
     * @param anneeNaissance Date de naissance de(s) l'élève(s)(l'année)
     * @return 1 liste d'élève ayant le même age.
     * @throws SQLException
     */
    public static ArrayList<Eleve> getListEleveByDateN(int anneeNaissance) throws SQLException {
        ArrayList<Eleve> listEleveAnneeNaissance = new ArrayList<Eleve>();
        //on recupere l'année en cours
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        //Variable à passer en paramètre dans la requête pour avoir les éléves d'une tranche d'age:
        //Je fais la différence entre l'année en cours et l'année passée en paramètre de la fonction.
        int anneeNaissanceEleves = year - anneeNaissance;
        String req = "SELECT num, no, nom, age, adresse FROM eleve WHERE age =" + anneeNaissanceEleves + " ";

        // Exécute la requête et remplis la liste passée en paramètre
        generateEleveList(req, listEleveAnneeNaissance);

        return listEleveAnneeNaissance;

    }


    /**
     * Récupère tous les élèves et en retourne une liste
     *
     * @return La liste de tous les élèves.
     * @throws SQLException
     */
    public static ArrayList<Eleve> getAllEleve() throws SQLException {

        ArrayList<Eleve> listEleve = new ArrayList<Eleve>();

        String req = "SELECT num, no, nom, age, adresse FROM eleve ";

        // Exécute la requête et remplis la liste passée en paramètre
        generateEleveList(req, listEleve);

        return listEleve;
    }


    /**
     * Effectue une connexion à la DB, exécute la requête et remplis la liste passée en paramètre avec les résultats
     *
     * @param req       requête à exécuter
     * @param listEleve Liste d'élève à remplir avec les résultats de la requête
     * @throws SQLException
     */
    public static void generateEleveList(String req, ArrayList<Eleve> listEleve) throws SQLException {

        // Connexion
        DBAction.DBConnexion();
        // exécution de la requête et init
        DBAction.setRes(DBAction.getStm().executeQuery(req));

        while (DBAction.getRes().next()) {
            //Instanciation de mon objet Eleve & Creation de l'objet eleveTemp à travers le ResultSet BD
            Eleve elevTemp = new Eleve(DBAction.getRes().getString(1), DBAction.getRes().getInt(2), DBAction.getRes().getString(3), DBAction.getRes().getInt(4), DBAction.getRes().getString(5));
            listEleve.add(elevTemp);
        }
        // Fermeture de la connexion
        DBAction.DBClose();
    }
}