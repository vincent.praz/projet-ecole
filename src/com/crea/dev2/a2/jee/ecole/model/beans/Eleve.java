package com.crea.dev2.a2.jee.ecole.model.beans;

public class Eleve {
    private String num;
    private int no;
    private String nom;
    private int age;
    private String adresse;

    public Eleve(){

    }

    /**
     * Constructeur de l'objet élève
     *
     * @param num numéro identifiant de l'élève
     * @param no numéro de chambre
     * @param nom nom de l'élève
     * @param age age de l'élève
     * @param adresse adresse de l'élève
     */
    public Eleve(String num, int no, String nom, int age, String adresse) {
        this.num = num;
        this.no = no;
        this.nom = nom;
        this.age = age;
        this.adresse = adresse;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void affiche() {
        System.out.println("num " + num + " No " + no + " nom " + nom + " age " + age + " adresse " + adresse);
    }
}
