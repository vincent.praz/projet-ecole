package com.crea.dev2.a2.jee.ecole.model.dao;

import com.crea.dev2.a2.jee.ecole.model.beans.Chambre;
import com.crea.dev2.a2.jee.ecole.model.utils.DBAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class ChambreDAO {

    /**
     * Liste un Chambre en particulier d'après son identifiant passé en paramètre de la fonction.
     *
     * @param no identifiant de la chambre
     * @return retourne l'objet Chambre
     * @throws SQLException
     */
    public static Chambre getChambreByNo(int no) throws SQLException {
        DBAction.DBConnexion(); // test conn
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM chambre WHERE no = " + no + ";");
        resultSet.first(); // Starting the resultset at the first position
        Chambre chambre = new Chambre(resultSet.getInt("no"), resultSet.getString("num"), resultSet.getFloat("prix"));
        resultSet.close(); // Fermeture du resultSet
        DBAction.DBClose();
        return chambre;
    }

    /**
     * Delete Chambre par un numéro d'identifiant délève
     *
     * @param no numéro de la chambre
     * @return : 1 ou 0 (le nbr de chambre supprimées) sinon le (-) code d'erreur
     */
    public static int deleteChambreByNo(int no) {
        DBAction.DBConnexion(); // test conn
        try {
            no = DBAction.getStm().executeUpdate("DELETE FROM chambre WHERE no = " + no + ";");
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getErrorCode();
        }
        DBAction.DBClose();
        return no;
    }

    /**
     * Ajouter une Chambre
     *
     * @param no   numéro de la chambre
     * @param num  numéro de identifiant de l'élève
     * @param prix prix de la chambre
     * @return 1 ou 0 (le nbr de chambre ajoutées) sinon le (-) code d'erreur
     */
    public static int addChambre(int no, String num, float prix) {
        DBAction.DBConnexion(); // test conn
        // Attention a bien mettre des '' pour les strings
        String SQL = "INSERT INTO chambre(no,num,prix) VALUES(" + no + ",'" + num + "'," + prix + ");";
        try {
            no = DBAction.getStm().executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            no = e.getErrorCode();
        }
        DBAction.DBClose();
        return no;
    }

    /**
     * Ajouter une Chambre
     *
     * @param no   Numéro de la chambre
     * @param prix prix de la chambre
     * @return 1 ou 0 (le nbr de chambre ajoutées) sinon le (-) code d'erreur
     */
    public static int addChambre(int no, float prix) {
        DBAction.DBConnexion(); // test conn
        // Attention a bien mettre des '' pour les strings
        String SQL = "INSERT INTO chambre(no,num,prix) VALUES(" + no + "," + null + "," + prix + ");";
        try {
            no = DBAction.getStm().executeUpdate(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
            no = e.getErrorCode();
        }
        DBAction.DBClose();
        return no;
    }

    /**
     * La liste des chambres
     *
     * @return : la liste de toutes les chambres.
     * @throws SQLException
     */
    public static Vector<Chambre> getAllChambre() throws SQLException {
        DBAction.DBConnexion(); // test conn
        Vector<Chambre> vChambre = new Vector<>();
        ResultSet resultSet = DBAction.getStm().executeQuery("SELECT * FROM chambre;");
        while (resultSet.next()) {
            Chambre chambre = new Chambre(resultSet.getInt("no"), resultSet.getString("num"), resultSet.getFloat("prix"));
            vChambre.add(chambre);
        }
        DBAction.DBClose();
        return vChambre;
    }


    /**
     * Met à jour le prix d'une chambre par son numéro
     *
     * @param num  numéro identifiant de l'élève
     * @param prix prix de la chambre
     * @return 1 ou 0  (le nbr de chambre mis à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updatePrixByNum(String num, float prix)
            throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE chambre SET prix = " + prix + " WHERE num ='" + num + "' ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Met a jour le num élève selon le no chambre
     *
     * @param num numéro identifiant de l'élève
     * @param no  numéro de chambre
     * @return 1 ou 0  (le nbr de chambres mises à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updateNumByNo(String num, int no)
            throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE chambre SET num = '" + num + "' WHERE no = " + no + " ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }


    /**
     * Met à null le num d'élève pour une chambre selon le no de chambre
     *
     * @param no numéro de chambre
     * @return 1 ou 0  (le nbr de chambres mises à jour) sinon le (-) code d'erreur
     * @throws SQLException
     */
    public static int updateNumByNo(int no)
            throws SQLException {
        int result = -1;
        DBAction.DBConnexion();
        String req = "UPDATE chambre SET num = " + null + " WHERE no = " + no + " ";
        result = DBAction.getStm().executeUpdate(req);
        System.out.println("Requete executee");

        DBAction.DBClose();
        return result;
    }
}
